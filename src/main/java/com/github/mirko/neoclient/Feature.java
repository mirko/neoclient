package com.github.mirko.neoclient;

public abstract class Feature {
    abstract public void onInitialize();
}
