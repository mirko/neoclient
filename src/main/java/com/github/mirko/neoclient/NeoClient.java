package com.github.mirko.neoclient;

import com.github.mirko.neoclient.feature.Commands;
import com.github.mirko.neoclient.feature.Core;
import com.github.mirko.neoclient.feature.NeoHash;
import com.github.mirko.neoclient.feature.SelfCare;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import net.fabricmc.api.ClientModInitializer;
import net.minecraft.client.MinecraftClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map;

public class NeoClient implements ClientModInitializer {
    public static Logger l = LoggerFactory.getLogger("neoclient");

    public static boolean coreEnabled = true;
    public static String neoBotKey = "";
    public static MinecraftClient mc = MinecraftClient.getInstance();

    public static ArrayList<Feature> features = new ArrayList<>();

    @Override
    public void onInitializeClient() {
        l.info("--------------");
        l.info("neoclient v0.1");
        l.info("");
        l.info("pre init");
        Path confPath = Paths.get("nerd.json");
        if(Files.exists(confPath)) {
            try {
                Gson gson = new Gson();
                Map<String, Object> sugmoa = gson.fromJson(Files.readString(confPath), new TypeToken<Map<String, Object>>() {}.getType());
                coreEnabled = !sugmoa.containsKey("coreEnabled") || (boolean) sugmoa.get("coreEnabled");
                neoBotKey = sugmoa.containsKey("neoBotKey") ? (String) sugmoa.get("neoBotKey") : "";
            } catch (IOException e) {
                l.error("Error while reading config: " + e);
                l.error("Not reading config.");
            }
        }
        features.add(Core.INSTANCE);
        features.add(Commands.INSTANCE);
        features.add(NeoHash.INSTANCE);
        features.add(SelfCare.INSTANCE);
        features.forEach(Feature::onInitialize);
        l.info("--------------");
    }
}
