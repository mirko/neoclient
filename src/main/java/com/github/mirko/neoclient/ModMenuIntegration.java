package com.github.mirko.neoclient;

import com.google.gson.Gson;
import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;
import me.shedaniel.clothconfig2.api.ConfigBuilder;
import me.shedaniel.clothconfig2.api.ConfigCategory;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import net.minecraft.text.Text;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

public class ModMenuIntegration implements ModMenuApi {
    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return parent -> {
            ConfigBuilder builder = ConfigBuilder.create().setParentScreen(parent).setTitle(Text.of("neoclient"));

            ConfigEntryBuilder entryBuilder = builder.entryBuilder();
            ConfigCategory general = builder.getOrCreateCategory(Text.of("General"));
            general.addEntry(entryBuilder.startBooleanToggle(Text.of("Enable core"), NeoClient.coreEnabled).setTooltip(Text.of("yk the command block stuff")).setSaveConsumer((r) -> NeoClient.coreEnabled = r).build()).addEntry(entryBuilder.startStrField(Text.of("neobot key"), NeoClient.neoBotKey).setSaveConsumer(s -> NeoClient.neoBotKey = s).build());
            builder.setSavingRunnable(() -> {
                HashMap<String, Object> toSerialize = new HashMap<>();
                toSerialize.put("neoBotKey", NeoClient.neoBotKey);
                toSerialize.put("coreEnabled", NeoClient.coreEnabled);
                Gson gson = new Gson();
                try {
                    Path confPath = Path.of("nerd.json"); // i guess i can try the new path.of api idk i dont use it normally
                    if(!Files.exists(confPath)) {
                        Files.createFile(confPath);
                    }
                    // bruh why are my fans so high i am like just only coding and i feel like the laptop has -3 degrees celsius
                    Files.writeString(confPath, gson.toJson(toSerialize), StandardCharsets.UTF_8);
                } catch (IOException e) {
                    NeoClient.l.error(e.toString()); // wonder what could go wrong here idk
                }
            }); return builder.build();
        };
    }
}
