package com.github.mirko.neoclient.mixin;

import com.github.mirko.neoclient.NeoClient;
import com.github.mirko.neoclient.feature.Core;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.entity.MovementType;
import net.minecraft.util.math.Position;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import static com.github.mirko.neoclient.NeoClient.mc;
import static org.spongepowered.asm.mixin.MixinEnvironment.Side.CLIENT;

@Mixin(ClientPlayerEntity.class)
public class ClientPlayerEntityMixin {
    @Inject(method = "move", at = @At("HEAD"))
    void move(MovementType movementType, Vec3d movement, CallbackInfo ci) {
        if((Object) this != mc.player) {
            NeoClient.l.error("this != mc.player");
            return;
        }
        Vec3d newPos = ((ClientPlayerEntity) (Object) this).getPos().add(movement);
        Vec3i corePos = Core.INSTANCE.getPos();
//        NeoClient.l.info(String.valueOf((corePos.getX() - newPos.x)));
//        NeoClient.l.info(String.valueOf((corePos.getY() - newPos.y)));
//        NeoClient.l.info(String.valueOf((corePos.getZ() - newPos.z)));
        if(corePos.getX() - newPos.x > 100 ||
                corePos.getX() - newPos.x < -100 ||
                corePos.getZ() - newPos.z > 100 ||
                corePos.getZ() - newPos.z < -100) {
            Core.INSTANCE.move(newPos).refill();
        }
    }
}
