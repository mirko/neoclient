package com.github.mirko.neoclient.mixin;

import com.github.mirko.neoclient.feature.Commands;
import com.github.mirko.neoclient.feature.Core;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.minimessage.MiniMessage;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import net.minecraft.client.gui.screen.ChatScreen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.util.Uuids;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.github.mirko.neoclient.NeoClient.mc;

@Mixin(ChatScreen.class)
public class ChatScreenMixin {
    @Shadow
    protected TextFieldWidget chatField;

    @Inject(method = "sendMessage", at = @At("HEAD"), cancellable = true)
    public void sendMessage(String chatText, boolean addToHistory, CallbackInfoReturnable<Boolean> cir) {

        if (chatText.startsWith(Commands.INSTANCE.prefix)) {
            Commands.INSTANCE.executeCommand(chatText.substring(Commands.INSTANCE.prefix.length()));
            if (addToHistory) mc.inGameHud.getChatHud().addToMessageHistory(chatText);
            cir.setReturnValue(true);
        } else if(!chatText.isBlank()) {
            ArrayList<Character> list = new ArrayList<>();

            list.add('/');
            list.add('*');
            // ★
            if (!list.contains(chatText.charAt(0))) {
                Core.INSTANCE.run("minecraft:tellraw @a " + GsonComponentSerializer.gson().serialize(Component.translatable("%s %s › %s").args(
//                        MiniMessage.miniMessage().deserialize("<dark_gray>[<gradient:#00d2ff:#928DAB>neoclient</gradient><dark_gray>]"),
                        MiniMessage.miniMessage().deserialize("<red>★ KCP <dark_gray>|"),
//                        MiniMessage.miniMessage().deserialize("|"),
                        Component.text(mc.getSession().getProfile().getName()).color(NamedTextColor.RED),
                        MiniMessage.miniMessage().deserialize(chatText).color(NamedTextColor.WHITE)
                ).color(NamedTextColor.DARK_GRAY)));
                if (addToHistory) mc.inGameHud.getChatHud().addToMessageHistory(chatText);
                cir.setReturnValue(true);
                cir.cancel();
            }
        }

    }
    @Inject(at = @At(value = "TAIL"), method = "init")
    public void fard (CallbackInfo cir) {
        chatField.setMaxLength(Integer.MAX_VALUE);
    }
}
