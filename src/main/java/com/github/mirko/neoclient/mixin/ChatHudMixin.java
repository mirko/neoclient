package com.github.mirko.neoclient.mixin;

import net.minecraft.client.gui.hud.ChatHud;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableTextContent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Objects;

@Mixin(ChatHud.class)
public class ChatHudMixin {
    @Inject(method = "addMessage(Lnet/minecraft/text/Text;)V", at=@At("HEAD"), cancellable = true)
        void addMessage(Text message, CallbackInfo ci) {
            if(message.getContent() instanceof TranslatableTextContent) {
                if(Objects.equals(((TranslatableTextContent) message.getContent()).getKey(), "advMode.setCommand.success")) {
                    ci.cancel();
                }
            }
        }
}
