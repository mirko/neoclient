package com.github.mirko.neoclient.feature;

import com.github.mirko.neoclient.Feature;
import com.github.mirko.neoclient.NeoClient;
import com.github.mirko.neoclient.Utils;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.arguments.StringArgumentType;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.rendering.v1.BuiltinItemRendererRegistry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtString;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;

import java.util.Objects;

import static com.github.mirko.neoclient.NeoClient.l;
import static com.github.mirko.neoclient.NeoClient.mc;

public class NeoHash extends Feature {

    public String prevHash = "";
    public static NeoHash INSTANCE = new NeoHash();

    @Override
    public void onInitialize() {
        l.info("☭");
        ClientTickEvents.END_WORLD_TICK.register(world -> {
            assert mc.player != null;
            if(mc.player.getInventory().offHand.isEmpty()) return;
            NbtCompound nbt = mc.player.getInventory().offHand.get(0).getNbt();
            if(nbt != null && nbt.contains("ntr")) {
                l.info("New hash: " + Objects.requireNonNull(nbt.get("ntr")).asString());
                prevHash = Objects.requireNonNull(nbt.get("ntr")).asString();
                mc.player.getInventory().offHand.set(0, new ItemStack(Registries.ITEM.get(0), 0));
            }
        });
        Commands.INSTANCE.dispatcher.register(
                Commands.literal("nval").then(
                        Commands.argument("command", StringArgumentType.greedyString()).executes(c -> {
                            prevHash = Utils.generateNewHash(prevHash, NeoClient.neoBotKey);
                            Objects.requireNonNull(mc.getNetworkHandler()).sendChatMessage(
                                    "_" + c.getArgument("command", String.class) + " " + prevHash
                            );
                            return Command.SINGLE_SUCCESS;
                        })
                )
        );
    }
}
