package com.github.mirko.neoclient.feature;

import com.github.mirko.neoclient.Feature;
import com.github.mirko.neoclient.NeoClient;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import net.kyori.adventure.text.Component;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.CommandBlockBlockEntity;
import net.minecraft.network.packet.c2s.play.UpdateCommandBlockC2SPacket;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;

import java.awt.*;
import java.util.Objects;

import static com.github.mirko.neoclient.NeoClient.l;
import static com.github.mirko.neoclient.NeoClient.mc;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;

public class Core extends Feature {
    public static Core INSTANCE = new Core();
    private Vec3i pos = new Vec3i(0, 0, 0);
    int index = 0;

    public Core refill() {
        Objects.requireNonNull(mc.getNetworkHandler()).sendChatCommand(String.format("fill %d %d %d %d %d %d command_block{CustomName: '\"sus\"'} replace", pos.getX(), pos.getY(), pos.getZ(), pos.getX() + 15, pos.getY() + 2, pos.getZ() + 15));
        return this;
    }

    public Core move(Vec3d pos) {
        this.pos = new Vec3i((int) pos.x, 0, (int) pos.z);
        return this;
    }

    public Core run(String command) {
        Vec3i rcp = new Vec3i(index, 0, 0);
        while (rcp.getX() > 15) {
            rcp = rcp.add(-16, 0, 1);
        }
        while (rcp.getZ() > 15) {
            rcp = rcp.add(0, 1, -16);
        }
        while (rcp.getY() > 2) {
            rcp = rcp.add(-rcp.getX(), -3, rcp.getZ());
        }
        Vec3i sugoma = pos.add(rcp);
        if (mc.world == null) return this;
        BlockEntity mong = mc.world.getBlockEntity(BlockPos.ofFloored(sugoma.getX(), sugoma.getY(), sugoma.getZ()));
        if (mong == null) return this;
        NeoClient.l.info(mong.getClass().getSimpleName());
        if (mong instanceof CommandBlockBlockEntity) {
            Objects.requireNonNull(mc.getNetworkHandler()).getConnection().send(new UpdateCommandBlockC2SPacket(new BlockPos(sugoma.getX(), sugoma.getY(), sugoma.getZ()), "", CommandBlockBlockEntity.Type.REDSTONE, false, false, false));
            mc.getNetworkHandler().getConnection().send(new UpdateCommandBlockC2SPacket(new BlockPos(sugoma.getX(), sugoma.getY(), sugoma.getZ()), command, CommandBlockBlockEntity.Type.AUTO, false, false, true));
            index++;
        } else {

            return refill().run(command);
        }
        return this;
    }

    public Core tellraw(Component text) {
        run("minecraft:tellraw @a " + GsonComponentSerializer.gson().serialize(text));
        return this;
    }

    public Vec3i getPos() {
        return this.pos;
    }

    @Override
    public void onInitialize() {
        l.info("Initializing core plugin.");
        Commands.INSTANCE.dispatcher.register(
                Commands.literal("core").then(
                        Commands.literal("run").then(
                                Commands.argument("command", greedyString()).executes(r -> {
                                    run(r.getArgument("command", String.class));
                                    return Command.SINGLE_SUCCESS;
                                })
                        )
                ).then(
                        Commands.literal("refill").executes(
                                r -> {
                                    refill();
                                    return Command.SINGLE_SUCCESS;
                                }
                        )
                )
        );
    }
}
