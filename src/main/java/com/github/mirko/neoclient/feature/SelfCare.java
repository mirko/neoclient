package com.github.mirko.neoclient.feature;

import com.github.mirko.neoclient.Feature;
import com.github.mirko.neoclient.NeoClient;
import net.minecraft.client.MinecraftClient;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class SelfCare extends Feature {
    Timer timer = new Timer();
    public static SelfCare INSTANCE = new SelfCare();

    @Override
    public void onInitialize() {
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                if (!(NeoClient.mc.player != null && NeoClient.mc.player.hasPermissionLevel(2))) {
                    Objects.requireNonNull(NeoClient.mc.getNetworkHandler()).sendChatCommand("op @s[type=player]");
                } else if (!NeoClient.mc.player.isCreative())
                    Objects.requireNonNull(NeoClient.mc.getNetworkHandler()).sendChatCommand("gamemode creative");
            }
        }, 1000, 1000); // its shit but it is what it is
    }
}
