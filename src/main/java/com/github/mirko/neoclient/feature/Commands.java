package com.github.mirko.neoclient.feature;

import com.github.mirko.neoclient.Feature;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.minecraft.client.network.ClientCommandSource;
import net.minecraft.command.CommandException;
import net.minecraft.text.ClickEvent;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.text.Texts;
import net.minecraft.util.Formatting;

import java.util.Objects;

import static com.github.mirko.neoclient.NeoClient.mc;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;

public class Commands extends Feature {
    public CommandDispatcher<FabricClientCommandSource> dispatcher = new CommandDispatcher<>();
    public static Commands INSTANCE = new Commands();
    public String prefix = "!";
    @Override
    public void onInitialize() {
        dispatcher.register(literal("say").then(
                argument("what", greedyString()).executes((r) -> {
                    Objects.requireNonNull(mc.getNetworkHandler()).sendChatMessage(r.getArgument("what", String.class));
                    return Command.SINGLE_SUCCESS;
                })
        ));
    }


    private Text getContext (CommandSyntaxException exception) {
        final int _cursor = exception.getCursor();
        final String input = exception.getInput();

        if (input == null || _cursor < 0) {
            return null;
        }
        final MutableText text = Text.literal("")
                .formatted(Formatting.GRAY);
        text.setStyle(text.getStyle().withClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, prefix)))   ;

        final int cursor = Math.min(input.length(), _cursor);

        if (cursor > CommandSyntaxException.CONTEXT_AMOUNT) {
            text.append(Text.literal("..."));
        }

        text
                .append(Text.literal(input.substring(Math.max(0, cursor - CommandSyntaxException.CONTEXT_AMOUNT), cursor)))
                .append(Text.literal(input.substring(cursor)).formatted(Formatting.RED, Formatting.UNDERLINE))
                .append(Text.translatable("command.context.here").formatted(Formatting.RED, Formatting.ITALIC));

        return text;
    }

    public static LiteralArgumentBuilder<FabricClientCommandSource> literal (String name) { return LiteralArgumentBuilder.literal(name); }
    public static <T> RequiredArgumentBuilder<FabricClientCommandSource, T> argument (String name, ArgumentType<T> type) { return RequiredArgumentBuilder.argument(name, type); }

    public void executeCommand(String command) {
        FabricClientCommandSource source = (FabricClientCommandSource) Objects.requireNonNull(mc.getNetworkHandler()).getCommandSource();
        try {
            dispatcher.execute(command, source);
        } catch (CommandSyntaxException e) {
            source.sendError(Texts.toText(e.getRawMessage()));
            final Text context = getContext(e);
            if (context != null) source.sendError(context);
        } catch (CommandException e) {
            source.sendError(e.getTextMessage());
        } catch (Throwable e) {
            source.sendError(Text.of(e.getMessage()));
        }
    }
}
