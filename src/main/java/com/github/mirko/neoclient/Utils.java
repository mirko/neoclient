package com.github.mirko.neoclient;

import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

public class Utils {
    public static String generateNewHash(String oldHash, String key) {
        return Hashing.sha256().hashString(oldHash + key, StandardCharsets.UTF_8).toString().substring(0, 16);
    }
}
